#include <SFML/Graphics.hpp>
#include "player.hpp"
#include "enemy.hpp"

namespace temp {
    class Game {
    public:
        Game();
        ~Game();

        void update();
        void render(sf::RenderWindow &window);

    private:
        Player player;
        Enemy enemy;
    };
}