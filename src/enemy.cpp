#include "enemy.hpp"
#include "player.hpp"

Enemy::Enemy(): player(nullptr){
    shape.setPosition({ 50, 50 });
    shape.setFillColor(sf::Color::Red);
}

void Enemy::update(){
    move();

    if(player == nullptr){
        return;
    }

    shape.setFillColor(sf::Color::Red);
    if(shape.getGlobalBounds().intersects(player->getShape().getGlobalBounds())){
        shape.setFillColor(sf::Color::Blue);
    }
}

void Enemy::setPlayer(Player *player){
    this->player = player;
}

void Enemy::move(){
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
        shape.move(-0.01, 0);
        return;
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
        shape.move(0.01, 0);
        return;
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
        shape.move(0, -0.01);
        return;
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
        shape.move(0, 0.01);
        return;
    }
}