#pragma once
#include "entity.hpp"

class Enemy;

class Player : public Entity {
public:
    Player();

    void update();

    void setEnemy(Enemy *enemy);

private:
    void move();

    Enemy *enemy;
};