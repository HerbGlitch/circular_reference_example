#include "player.hpp"
#include "enemy.hpp"

Player::Player(): enemy(nullptr){
    shape.setPosition(0, 0);
    shape.setFillColor(sf::Color::Green);
}

void Player::update(){
    move();

    if(enemy == nullptr){
        return;
    }

    shape.setFillColor(sf::Color::Green);
    if(shape.getGlobalBounds().intersects(enemy->getShape().getGlobalBounds())){
        shape.setFillColor(sf::Color::White);
    }
}

void Player::setEnemy(Enemy *enemy){
    this->enemy = enemy;
}

void Player::move(){
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
        shape.move(-0.01, 0);
        return;
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
        shape.move(0.01, 0);
        return;
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
        shape.move(0, -0.01);
        return;
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
        shape.move(0, 0.01);
        return;
    }
}