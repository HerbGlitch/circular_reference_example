#include "entity.hpp"

Entity::Entity(){
    shape.setSize({ 20, 20 });
}

Entity::~Entity(){
}

void Entity::update(){
}

void Entity::render(sf::RenderWindow &window){
    window.draw(shape);
}

sf::RectangleShape Entity::getShape(){
    return shape;
}