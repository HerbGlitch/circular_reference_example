#pragma once
#include <SFML/Graphics.hpp>

class Entity {
public:
    Entity();
    virtual ~Entity();

    virtual void update();
    virtual void render(sf::RenderWindow &window);

    sf::RectangleShape getShape();

protected:
    sf::RectangleShape shape;
};