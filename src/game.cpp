#include "game.hpp"

namespace temp {
    Game::Game(){
        player = Player();
        enemy = Enemy();

        player.setEnemy(&enemy);
        enemy.setPlayer(&player);
    }

    Game::~Game(){}

    void Game::update(){
        player.update();
        enemy.update();
    }

    void Game::render(sf::RenderWindow &window){
        player.render(window);
        enemy.render(window);
    }
}
