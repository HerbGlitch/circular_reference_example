#pragma once
#include "entity.hpp"

class Player;

class Enemy : public Entity {
public:
    Enemy();

    void update();

    void setPlayer(Player *player);

private:
    void move();

    Player *player;
};